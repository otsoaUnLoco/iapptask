package com.dawid.iappstask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class IAppTaskApp : Application() {

}
