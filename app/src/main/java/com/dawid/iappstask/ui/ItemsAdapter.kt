package com.dawid.iappstask.ui

import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.dawid.iappstask.databinding.ItemLayoutBinding
import com.dawid.iappstask.domain.Item

class ItemsAdapter(
    private val onClick: (Item) -> Unit,
) : ListAdapter<Item, ItemsAdapter.ItemsViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder =
        ItemsViewHolder(
            ItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, onClick)
    }

    class ItemsViewHolder(private val layout: ItemLayoutBinding) : ViewHolder(layout.root) {
        fun bind(item: Item, onClick: (Item) -> Unit) {
            layout.tvDescription.text = Html.fromHtml(item.description, Html.FROM_HTML_MODE_COMPACT)
            layout.ivItemImage.load(item.imageUrl)
            layout.root.setOnClickListener { onClick(item) }
        }
    }

    private companion object DiffCallback : DiffUtil.ItemCallback<Item>() {
        override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean = oldItem == newItem
        override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean = oldItem == newItem
    }
}
