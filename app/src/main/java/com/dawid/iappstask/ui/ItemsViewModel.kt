package com.dawid.iappstask.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dawid.iappstask.domain.FetchItemsUseCase
import com.dawid.iappstask.domain.GetSortedItemsUseCase
import com.dawid.iappstask.domain.Item
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ItemsViewModel @Inject constructor(
    private val getSortedItemsUseCase: GetSortedItemsUseCase,
    private val fetchItemsUseCase: FetchItemsUseCase,
) : ViewModel() {

    private val _state = MutableStateFlow(ViewState())
    val state: StateFlow<ViewState> get() = _state

    private var job: Job? = null

    fun fetchData(): Job {
        if (job == null) {
            job = listenToItems()
        }

        return viewModelScope.launch {
            emit { copy(inProgress = true) }
            val response = withContext(Dispatchers.IO) { fetchItemsUseCase() }
            emit { copy(inProgress = false, hasNetworkError = response.isFailure) }
        }
    }

    fun onErrorHandled() {
        emit { copy(hasNetworkError = false) }
    }

    private fun emit(reducer: ViewState.() -> ViewState) {
        _state.update { it.reducer() }
    }

    private fun listenToItems(): Job = viewModelScope.launch {
        getSortedItemsUseCase()
            .collect { items ->
                emit { copy(items = items, inProgress = false) }
            }
    }
}

data class ViewState(
    val inProgress: Boolean = false,
    // TODO differentiate errors
    val hasNetworkError: Boolean = false,
    val items: List<Item> = emptyList(),
)
