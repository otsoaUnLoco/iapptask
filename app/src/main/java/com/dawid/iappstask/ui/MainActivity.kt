package com.dawid.iappstask.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.dawid.iappstask.R
import com.dawid.iappstask.databinding.ActivityMainBinding
import com.dawid.iappstask.domain.Item
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val vm: ItemsViewModel by viewModels()
    private val adapter: ItemsAdapter by lazy { ItemsAdapter(::onClick) }
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupUi()
        vm.fetchData()
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                vm.state.collect(::updateUi)
            }
        }
    }

    override fun onDestroy() {
        binding.rvItems.adapter = null
        super.onDestroy()
    }

    private fun setupUi() {
        binding.rvItems.adapter = adapter
        binding.rvItems.addItemDecoration(
            DividerItemDecoration(
                this@MainActivity,
                LinearLayoutManager.VERTICAL
            )
        )
    }

    private fun updateUi(viewState: ViewState) {
        adapter.submitList(viewState.items)
        if (viewState.inProgress) {
            binding.progressCircular.show()
        } else {
            binding.progressCircular.hide()
        }
        if (viewState.hasNetworkError) {
            Toast.makeText(this, getText(R.string.network_error), Toast.LENGTH_LONG).show()
            vm.onErrorHandled()
        }
    }

    private fun onClick(item: Item) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(item.link))
        startActivity(browserIntent)
    }
}
