package com.dawid.iappstask.di

import android.content.Context
import androidx.room.Room
import com.dawid.iappstask.BuildConfig
import com.dawid.iappstask.room.AppDatabase
import com.dawid.iappstask.data.DefaultRepository
import com.dawid.iappstask.data.database.ItemDao
import com.dawid.iappstask.data.network.Api
import com.dawid.iappstask.domain.Repository
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ItemModule {
    @Provides
    fun provideOkhttp(): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
        .build()

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val contentType = "application/json".toMediaType()
        val json = Json { ignoreUnknownKeys = true }
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(okHttpClient)
            .addConverterFactory(json.asConverterFactory(contentType))
            .build()
    }

    @Provides
    fun provideApi(retrofit: Retrofit): Api = retrofit.create()

    @Provides
    @Singleton
    fun provideRoomDb(@ApplicationContext applicationContext: Context): AppDatabase =
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database"
        ).build()

    @Provides
    fun provideDao(appDatabase: AppDatabase): ItemDao = appDatabase.itemDao()

    @Provides
    @Singleton
    fun provideRepository(defaultRepository: DefaultRepository): Repository = defaultRepository
}
