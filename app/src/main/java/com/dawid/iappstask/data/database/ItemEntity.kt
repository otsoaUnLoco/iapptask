package com.dawid.iappstask.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dawid.iappstask.domain.Item
import java.time.LocalDateTime

@Entity(tableName = "Item")
data class ItemEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    override val link: String,
    override val description: String,
    @ColumnInfo("image_url") override val imageUrl: String,
    @ColumnInfo("created_at") override val publishedAt: LocalDateTime,
) : Item {
    companion object {
        fun fromItem(item: Item): ItemEntity = ItemEntity(
            link = item.link,
            description = item.description,
            publishedAt = item.publishedAt,
            imageUrl = item.imageUrl
        )
    }
}
