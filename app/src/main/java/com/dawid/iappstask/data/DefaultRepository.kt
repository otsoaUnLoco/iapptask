package com.dawid.iappstask.data

import com.dawid.iappstask.data.database.ItemDao
import com.dawid.iappstask.data.database.ItemEntity
import com.dawid.iappstask.data.network.Api
import com.dawid.iappstask.domain.Item
import com.dawid.iappstask.domain.Repository
import kotlinx.coroutines.flow.Flow
import java.io.IOException
import javax.inject.Inject

class DefaultRepository @Inject constructor(
    private val api: Api,
    private val itemDao: ItemDao
) : Repository {
    override fun getItems(): Flow<List<Item>> = itemDao.getAll()

    override suspend fun fetchNewItems(): Result<Unit> = try {
        val response = api.getItems()
        val entities = response.items.map { item -> ItemEntity.fromItem(item) }
        itemDao.clear()
        itemDao.insertItems(*entities.toTypedArray())
        Result.success(Unit)
    } catch (e: IOException) {
        Result.failure(e)
    }
}
