package com.dawid.iappstask.data.network

import com.dawid.iappstask.domain.Item
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Serializable
data class ApiResponse(
    val title: String,
    val link: String,
    val description: String,
    val modified: String,
    val generator: String,
    val items: List<ApiItem>,
)

@Serializable
data class ApiItem(
    val title: String,
    override val link: String,
    val media: Media,
    @SerialName("date_taken")
    val dateTaken: String,
    override val description: String,
    val published: String,
    val author: String,
    @SerialName("author_id")
    val authorId: String,
    val tags: String,
) : Item {
    override val imageUrl: String get() = media.m
    override val publishedAt: LocalDateTime get() {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
        return LocalDateTime.parse(published, formatter)
    }
}

@Serializable
data class Media(
    val m: String,
)
