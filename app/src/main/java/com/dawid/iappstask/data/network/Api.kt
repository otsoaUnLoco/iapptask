package com.dawid.iappstask.data.network

import retrofit2.http.GET

interface Api {
    @GET("services/feeds/photos_public.gne?format=json&tags=cat&nojsoncallback=1")
    suspend fun getItems(): ApiResponse
}
