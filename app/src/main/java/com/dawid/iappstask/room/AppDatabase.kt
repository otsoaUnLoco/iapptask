package com.dawid.iappstask.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dawid.iappstask.data.database.ItemDao
import com.dawid.iappstask.data.database.ItemEntity

@Database(entities = [ItemEntity::class], version = 1)
@TypeConverters(LocalDateTimeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun itemDao(): ItemDao
}
