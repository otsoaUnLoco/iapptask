package com.dawid.iappstask.domain

import java.time.LocalDateTime

interface Item {
    val imageUrl: String
    val description: String
    val link: String
    val publishedAt: LocalDateTime
    override fun equals(other: Any?): Boolean
    override fun hashCode(): Int
}
