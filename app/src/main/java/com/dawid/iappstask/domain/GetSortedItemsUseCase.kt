package com.dawid.iappstask.domain

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetSortedItemsUseCase @Inject constructor(
    private val repository: Repository,
) {

    operator fun invoke(): Flow<List<Item>> =
        repository.getItems().map { items ->
            items.sortedBy { item -> item.publishedAt }
        }
}
