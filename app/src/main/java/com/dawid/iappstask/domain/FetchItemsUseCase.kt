package com.dawid.iappstask.domain

import javax.inject.Inject

class FetchItemsUseCase @Inject constructor(
    private val repository: Repository,
) {

    suspend operator fun invoke() = repository.fetchNewItems()
}
