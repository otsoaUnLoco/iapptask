package com.dawid.iappstask.domain

import kotlinx.coroutines.flow.Flow

interface Repository {
    fun getItems(): Flow<List<Item>>

    suspend fun fetchNewItems(): Result<Unit>
}
